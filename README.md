# AudioStellar

Open source data-driven musical instrument for latent sound structure discovery and music experimentation

![screenshot](http://leandrogarber.info/proyectos/AudioStellar/audiostellar_screenshot_3.jpg)

Visualize a collection of short audio samples in an interactive 2D point map which enables to analyze 
resulting groups and play given samples in a novel way using various innovative music composition modes.


## Downloads

* [Ubuntu 18.04 v0.8.0](http://leandrogarber.info/proyectos/AudioStellar/AudioStellar_v0.8.0_Ubuntu.deb)
* [MacOS 64 bits v0.8.0](http://leandrogarber.info/proyectos/AudioStellar/AudioStellar_v0.8.0_MacOS.dmg)
* [Windows v0.8.0](http://leandrogarber.info/proyectos/AudioStellar/AudioStellar_v0.8.0_Win.zip)

If it is your first time downloading AudioStellar, make sure to also download one of our datasets below.


## Datasets

~~Current version of AudioStellar doesn't analize sound files, this is done in a [whole separate program](https://gitlab.com/ayrsd/audiostellar-data-analysis).~~
AudioStellar v0.8.0 can process any audio folder now. The results are not the best yet but are good enough. Feel free to contact us, send us sound files and we will run the optimized analysis and send you back the results.

You can also use the software using the datasets below.

### Drumkits (2129 audio files)
Samples of well known drum machines
[Download link](http://leandrogarber.info/proyectos/AudioStellar/dataset_drumkits.zip)

### Freesound 3seg (1992 audio files)
3 seconds random audio samples scrapped from FreeSounds
[Download link](http://leandrogarber.info/proyectos/AudioStellar/dataset_freesounds3seg.zip)

### Google's N-Synth: All C (4049 audio files)
All the C notes in Google's n-synth data set
[Download link](http://leandrogarber.info/proyectos/AudioStellar/dataset-nsynth-allC.zip)

### Household Objects (285 audio files)
Recordings of objects you would normally find at anyone's house.
[Download link](http://leandrogarber.info/proyectos/AudioStellar/dataset_householdobjects.zip)

### Anto (279 audio files)
Female singer's voice chopped in small audio samples
[Download link](http://leandrogarber.info/proyectos/AudioStellar/dataset_anto.zip)

### Drummachines (9594 audio files)
A lot of samples of uncategorized drum machines
[Download link](http://leandrogarber.info/proyectos/AudioStellar/dataset_drummachines.zip)

## Machine learning pipeline

![Pipeline](https://gitlab.com/ayrsd/audiostellar-data-analysis/raw/b0b058be8786b093b14e37feaa0880b3d4b091f5/proceso.png)

## Contribute

### How?

#### I'm a musician
* Make music with it
* Try new datasets, make your own!
* Give us feedback

#### I'm a programmer
* Fork it
* Hack it
* Browse our issues
* Open new issues
* Make it your own
* We love pull requests

### License

GNU/GPL v3

### How to compile

* Use openFrameworks 0.10.x
* Set OF_ROOT enviroment variable
* run install_addons.sh (for Windows use msys2)
* (MacOS ONLY) #include "ofMain.h" on addons/ofxConvexHull.h

## Future features

* Documentation
* MIDI full support (almost there!)
* OSC support
* VST implementation using JUCE (YES PLEASE!)
* LSTM for feature extraction
* Support for embedded devices (raspberry pi, bela)
* Android implementation

