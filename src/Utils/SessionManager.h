#pragma once

#include "ofMain.h"
#include "Utils.h"
#include "ofxJSON.h"
#include "Modes/Modes.h"
#include "Sound/Sounds.h"
#include "Utils/MidiServer.h"
#include "ofxImGui.h"



class SessionManager {

private:

    Modes * modes;
    Sounds * sounds;
    MidiServer * midiServer;

    bool sessionStarted = false;

    //RECENT PROJECTS
    vector<string> recentProjects;
    ofFile recentProjectsFile;
    vector<string> loadRecentProjects();
    void saveToRecentProject(string path);

    //LOAD JSON
    string jsonFilename = "";
    ofxJSONElement jsonFile;

    ofxJSONElement loadJsonTerminal(vector<string> args);
    ofxJSONElement loadJsonGui();
    ofxJSONElement loadJson(string path);

    void loadSessionFromJSON(ofxJSONElement jsonFile);
    ofxJSONElement parseJson(string jsonPath);

    void setWindowTitleWithSessionName();

public:

    SessionManager(Modes * _modes, Sounds * _sounds, MidiServer * _midiServer);


    void loadSession();//De gui
    void loadSession(vector<string> args);//De terminal
    void loadSession(string path);//de recent

    void saveSession();
    void saveAsNewSession();
    void drawGui();
    void exit();

    vector<string> getRecentProjects();
    bool areAnyRecentProjects(vector<string> recentProjects);
    bool getSessionStarted();
};
