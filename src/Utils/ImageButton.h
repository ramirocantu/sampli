#pragma once
#include "ofMain.h"

class ImageButton
{
public:
    float x;
    float y;
    ofImage * imgButton;
    ofImage * imgButtonActive;
    bool active;
    static int buttonSize;
    ofColor backgroundColor = ofColor(25);


    ImageButton( float x, float y, string path, string pathActive );
    void draw();
    void draw(float x, float y);
    bool click(float mouseX, float mouseY);
    bool getActive() const;
    void setActive(bool value);
};
