#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "Sounds.h"
#include "MidiServer.h"
#include "Modes.h"
#include "SessionManager.h"
#include "AudioDimensionalityReduction.h"
#include "GuiTheme.h"
#include "ofxJSON.h"
#include "Tooltip.h"

#define WINDOW_TOOLS_WIDTH 350
#define WINDOW_TOOLS_HEIGHT 600

class Gui {

private:

    AudioDimensionalityReduction adr;
    bool drawGui = true;

    SessionManager * sessionManager;
    Sounds * sounds;
    MidiServer * midiServer;
    Modes * modes;
    Tooltip * tooltip;

    ofxJSONElement jsonFile;

    bool isToolsWindowHovered = false;
    bool isWelcomeScreenOpen = true;
    bool isAboutScreenOpen = true;
    bool isDimReductScreenOpen = true;
    bool haveToDrawAboutScreen = false;
    bool haveToDrawDimReductScreen = false;
    bool haveToDrawFPS = false;
    
    const char* sample_rate_items[4] = { "11025", "22050", "44100", "48000" };
    int sample_rate_values[4] = { 11025, 22050, 44100, 48000 };
    int sample_rate_item_current = 1;
    
    const char* window_size_items[5] = { "512", "1024", "2048", "4096", "8192" };
    int window_size_values[5] = { 512, 1024, 2048, 4096, 8192 };
    int window_size_item_current = 2;
    
    const char* hop_size_items[5] = { "Window size ", "Window size / 2", "Window size / 4", "Window size / 8", "Window size / 16" };
    int hop_size_values[5] = { 1, 2, 4, 8, 16 };
    int hop_size_item_current = 2;
    
    const string welcomeScreenText =
            "The best way to start is by downloading one of our datasets, "
            "clicking the load button and selecting the downloaded '.json' file"
            "\n\n"
            "You can find all of our datasets at https://gitlab.com/ayrsd/audiostellar"
            "\n\n"
            "You can also generate a new sound map from your audio files clicking the new button below."
            "\n\n";

    void newSession();
    bool isProcessingFiles = false;

    void drawProcessingFilesWindow();
    bool drawWelcomeScreen();
    void drawMainMenu();
    void drawAboutScreen();
    void drawDimReductScreen();
    void drawFPS();

    ofTrueTypeFont font;
public:
    Gui(Sounds * sounds, MidiServer * midiServer, Modes * modes, SessionManager * sessionManager);
    ofxImGui::Gui gui;

    void draw();
    void keyPressed(int key);

    bool isMouseHoveringGUI();

};
