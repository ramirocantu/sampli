#include "Gui.h"

Gui::Gui(Sounds * sounds, MidiServer * midiServer, Modes * modes, SessionManager * sessionManager) {
    this->sounds = sounds;
    this->midiServer = midiServer;
    this->modes = modes;
    this->sessionManager = sessionManager;
    this->tooltip = new Tooltip();

    font.load( OF_TTF_MONO, 10 );

    gui.setup(new GuiTheme(), true);
    drawGui = true;
}


void Gui::newSession()
{
    haveToDrawDimReductScreen = true;
    isDimReductScreenOpen = true;
}

void Gui::drawProcessingFilesWindow()
{
    if ( isProcessingFiles ) {
        if ( adr.progress == 1.0f ) {
            isProcessingFiles = false;

            string resultJSONpath = AudioDimensionalityReduction::generatedJSONPath;
            if ( resultJSONpath != "" ) {
                sessionManager->loadSession(resultJSONpath);
            }
        } else {
            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200 ,100));

            bool isWindowOpen = true;

            string windowTitle = "Processing " +
                    ofToString(adr.filesFound) +
                    " files...";

            if ( ImGui::Begin( windowTitle.c_str(), &isWindowOpen, window_flags)) {
                ImGui::Text("Grab a coffee, "
                            "this can take quite a while.");

                ImGui::ProgressBar( adr.progress );
                ImGui::Text( adr.currentProcess.c_str() );

                ImGui::End();
            }
        }
    }
}

void Gui::draw() {

    gui.begin();

    //Menu Superior
    drawMainMenu();

    if(drawGui) {

        //Botones de seleccion de modo
        modes->drawModeSelector();

        //Tools window
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        auto mainSettings = ofxImGui::Settings();
        mainSettings.windowPos = ImVec2(ofGetWidth() - WINDOW_TOOLS_WIDTH - 20, 30);
        mainSettings.lockPosition = true;

        if ( isToolsWindowHovered )
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 1.f);
        else
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, .7f);
        if( ofxImGui::BeginWindow("Tools", mainSettings, window_flags)){
                ImGui::SetWindowSize( ImVec2(WINDOW_TOOLS_WIDTH,WINDOW_TOOLS_HEIGHT) );
                isToolsWindowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                              | ImGuiHoveredFlags_RootAndChildWindows);

                midiServer->SliderFloat("Master Volume", &sounds->volumen, 0.0f, 1.0f);
                ImGui::Checkbox("Midi Learn", &midiServer->midiLearn);
                modes->drawModesSettings();
        }
        ofxImGui::EndWindow(mainSettings);
        ImGui::PopStyleVar();

        Tooltip::drawGui();

        drawProcessingFilesWindow();

        drawAboutScreen();
        
        drawDimReductScreen();

    }

    if(!sessionManager->getSessionStarted()){
            drawWelcomeScreen();
    }

    drawFPS();

    gui.end();
}

bool Gui::drawWelcomeScreen(){

    ImGuiWindowFlags window_flags = 0;
    int w = 400;
    window_flags |= ImGuiWindowFlags_NoScrollbar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_NoNav;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
    ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));

    bool btn = false;

   if(isWelcomeScreenOpen){

       ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

       if (ImGui::Begin("Welcome to AudioStellar" , &isWelcomeScreenOpen, window_flags)){

           ImGui::PushTextWrapPos(w);
           ImGui::TextWrapped(welcomeScreenText.c_str());
           ImGui::Columns(2,NULL, false);

           if(ImGui::Button("New session")){
              newSession();
           }

           ImGui::NextColumn();

           if(ImGui::Button("Load session")){
                sessionManager->loadSession();
           }

           ImGui::End();
       }

       ImGui::PopStyleVar();
   }


   return btn;
}

void Gui::drawMainMenu(){
    if(ImGui::BeginMainMenuBar()){

        if(ImGui::BeginMenu("File")){

           if ( ImGui::MenuItem("New") ) { //"Ctrl+n"
               newSession();
           }

           if(ImGui::MenuItem("Open")){ //"Ctrl+o"
               sessionManager->loadSession();
           }

           if(ImGui::MenuItem("Save")){ // "Ctrl+s"
               sessionManager->saveSession();
           }

           if(ImGui::MenuItem("Save copy as...")){ //"Ctrl+Shift+s"
               sessionManager->saveAsNewSession();
           }

           if(ImGui::BeginMenu("Recent Projects")){
               vector<string> recentProjects = sessionManager->getRecentProjects();
               if(sessionManager->areAnyRecentProjects(recentProjects)){
                   for(unsigned int i = 0; i < recentProjects.size(); i++){
                       if(ImGui::MenuItem(recentProjects[i].c_str())){
                          sessionManager->loadSession(recentProjects[i].c_str());
                       }
                   }
               }else{
                   ImGui::PushStyleColor(ImGuiCol_Text,ImVec4(0.5,0.5,0.5,1.0));
                   ImGui::MenuItem("No recent Projects...");
                   ImGui::PopStyleColor();
               }
               ImGui::EndMenu();
           }

           if(ImGui::MenuItem("Close")){ //"Ctrl+q"
              exit(0);
           }

           ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("View")){
            ImGui::Checkbox("Show UI", &drawGui);
            ImGui::Checkbox("Show sound filenames", &sounds->showSoundFilenamesTooltip);
            ImGui::Checkbox("Show FPS", &haveToDrawFPS);
            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Settings")){
           sounds->drawGui();
           midiServer->drawGui();
           ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Help")){
            if(ImGui::MenuItem("About")){
                haveToDrawAboutScreen = true;
                isAboutScreenOpen = true;
            }
           ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }

}

void Gui::drawAboutScreen(){

    if(haveToDrawAboutScreen) {
        if(isAboutScreenOpen){
            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200, ofGetHeight()/3));
            if(ImGui::Begin("About", &isAboutScreenOpen)){
               ImGui::Text("Thanks for using AudioStellar v0.8.0. \n\n\nMore info at https://gitlab.com/ayrsd/audiostellar");
            }
            ImGui::End();
        }else{
            haveToDrawAboutScreen = false;
        }
    }
}

void Gui::drawDimReductScreen(){
    
    if(haveToDrawDimReductScreen) {
        
        if(isDimReductScreenOpen){
            
            ImGuiWindowFlags window_flags = 0;
            int w = 400;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));
            
            if (ImGui::Begin("Generate new session" , &isDimReductScreenOpen, window_flags)){
                
                //path
                ImGui::Text("Folder location");
                
                string folder;
                string containingFolder;
                string displayFolder;
                
                if(adr.dirPath.size()) {
                    folder = ofFilePath::getPathForDirectory(adr.dirPath);
                    containingFolder = ofFilePath::getEnclosingDirectory(adr.dirPath);
                    containingFolder = ofFilePath::getEnclosingDirectory(ofFilePath::removeTrailingSlash(containingFolder));
                    displayFolder =  ".../" + ofFilePath::makeRelative(containingFolder, folder);
                }
                
                ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,3));
                ImGui::BeginChild("child", ImVec2(208, 19), true);
                ImGui::TextUnformatted( displayFolder.c_str() );
                ImGui::EndChild();
                ImGui::PopStyleVar();
                
                ImGui::SameLine();
                if(ImGui::Button("Select folder")){
                    string path = AudioDimensionalityReduction::selectFolder();
                    if ( path.size() ) {
                        adr.dirPath = path;
                    }
                }
                ImGui::Text("");
                
                ImGui::Text("Audio");
                ImGui::Combo("Sample rate", &sample_rate_item_current, sample_rate_items, IM_ARRAYSIZE(sample_rate_items));
                adr.targetSampleRate = sample_rate_values[sample_rate_item_current];
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_SAMPLE_RATE);
                
                ImGui::InputFloat("Target Duration", &adr.targetDuration);
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_DURATION);
                ImGui::Text("");
                
                ImGui::Text("STFT");
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT);
                ImGui::Combo("Window size", &window_size_item_current, window_size_items, IM_ARRAYSIZE(window_size_items));
                adr.stft_windowSize = window_size_values[window_size_item_current];
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_WINDOW_SIZE);
                
                ImGui::Combo("Hop size", &hop_size_item_current, hop_size_items, IM_ARRAYSIZE(hop_size_items));
                adr.stft_hopSize = window_size_values[window_size_item_current] / hop_size_values[hop_size_item_current];
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_HOP_SIZE);
                ImGui::Text("");
                
                ImGui::Text("PCA");
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_PCA);
                ImGui::InputInt("PCA components", &adr.pca_nComponents);
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_PCA_COMPONENTS);
                ImGui::Text("");
                
                ImGui::Text("T-SNE");
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE);
                ImGui::InputDouble("Perplexity", &adr.tsne_perplexity);
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_PERPLEXITY);
                ImGui::InputDouble("Theta", &adr.tsne_theta);
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_THETA);
                ImGui::Text("");
                
                
                if(adr.dirPath.size()) {
                    if(ImGui::Button("Run")) {
                        haveToDrawDimReductScreen = false;
                        isProcessingFiles = true;
                        isWelcomeScreenOpen = false;
                        adr.startThread();
                    }
                }else{
                    ImVec4 color = ImColor(0.3f, 0.3f, 0.3f, 0.5f);
                    ImVec4 textColor = ImColor(1.f, 1.0f, 1.f, 0.5f);
                    ImGui::PushStyleColor(ImGuiCol_Text, textColor);
                    ImGui::PushStyleColor(ImGuiCol_Button, color);
                    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
                    ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);
                    ImGui::Button("Run");
                    ImGui::PopStyleColor(4);
                }
                ImGui::End();
            }
            ImGui::PopStyleVar();
            
        }
    }
}

void Gui::drawFPS()
{
    if ( haveToDrawFPS ) {
        ofSetColor(255);
        font.drawString(
            "FPS: " + ofToString(ofGetFrameRate(), 0),
            ofGetWidth() - 70,
            ofGetHeight() - 10);
    }
}

void Gui::keyPressed(int key) {
    if(key == 'g') {
        drawGui = !drawGui;
    }
}

bool Gui::isMouseHoveringGUI()
{
    return isToolsWindowHovered;
}

