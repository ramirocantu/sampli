#include "SessionManager.h"


SessionManager::SessionManager(Modes * _modes, Sounds * _sounds, MidiServer * _midiServer){

    modes = _modes;
    sounds = _sounds;
    midiServer = _midiServer;
    recentProjects = loadRecentProjects();

    //cargá el último
    if(areAnyRecentProjects(recentProjects)){
        loadSession(recentProjects[recentProjects.size() - 1]);
    }

}

void SessionManager::drawGui(){

    if(ImGui::Button("Save session")){
        saveSession();
    }

    if(ImGui::Button("Load")){
        loadSession();
    }

}

void SessionManager::loadSession() {
   ofxJSONElement json = loadJsonGui();
   if(json != Json::nullValue){
        loadSessionFromJSON(json);
   }

}

void SessionManager::loadSession(vector <string> args){
   ofxJSONElement json = loadJsonTerminal(args);
   loadSessionFromJSON(json);
}

void SessionManager::loadSession(string path){
    ofxJSONElement json = loadJson(path);
    loadSessionFromJSON(json);
}

void SessionManager::loadSessionFromJSON(ofxJSONElement file){

    //ESTO CARGA LOS SONIDOS
    sounds->loadSounds(file);

    Json::Value jsonModes = file["modes"];

    for ( int i = 0 ; i < modes->modes.size() ; i++ ) {
         modes->modes[i]->reset();
    }

    if ( jsonModes != Json::nullValue ) {
        for ( int i = 0 ; i < modes->modes.size() ; i++ ) {
            modes->modes[i]->load( jsonModes[ modes->modeNames[i] ] );
        }
    }
    midiServer->reset(); 
    midiServer->load(file["midiCC"]);

    //ESTO CARGA CONFIGURACIONES RELACIONADAS A LOS SONIDOS
    sounds->load(file["sounds"]);
    setWindowTitleWithSessionName();
}


ofxJSONElement SessionManager::loadJsonGui(){

    ofFileDialogResult selection =
        ofSystemLoadDialog("Select JSON file");

    ofxJSONElement file;

    if(selection.bSuccess) {
        string filename = selection.getPath();
        file = loadJson(filename);
    }

    return file;
}

ofxJSONElement SessionManager::loadJsonTerminal(vector<string> args) {

    ofxJSONElement file;
    bool osx = false;
    #ifdef TARGET_OSX
    osx = true;
    #endif

    if(args.size() > 1 && !osx) {
        string filename = args.at(1);
        file = loadJson(filename);
    } else {
        ofLogNotice("ERROR", "Bad Arguments or mac not supported :/");
        ofExit(1);
    }

    return file;
}

ofxJSONElement SessionManager::loadJson(string path){
        string ext = ofFilePath::getFileExt(path);

        ofxJSONElement file;

        if(ext != "json"){
          ofLogNotice("OOPS", "This is not a JSON file");
        }else{
          ofxJSONElement tempFile;
          tempFile = parseJson(path);
          
          //Check if minimum fields exist
          Json::Value soundFilesPath = tempFile["audioFilesPath"];
          Json::Value map = tempFile["tsv"];
          
          if(soundFilesPath == Json::nullValue || map == Json::nullValue){
              ofLogNotice("OOPS", "not a valid sound map");
          }else{
              file = tempFile;
              saveToRecentProject(path);
              sessionStarted = true;
          }
        }

        return file;

}

ofxJSONElement SessionManager::parseJson(string path){
    ofxJSONElement file;
    bool loadedJson = file.open(path);

    //Seteo las dos globales de la clase
    jsonFilename = path;
    jsonFile = file;

    if(!loadedJson) {
        ofLogNotice("ERROR", "problemas al cargar el json");
        ofExit(1);
    }

    file["audioFilesPath"] = Utils::resolvePath
                                 (path, file["audioFilesPath"].asString());
    return file;
}

vector<string> SessionManager::loadRecentProjects(){
    vector<string> recent;

    if(!ofFile::doesFileExist("recent.txt",true)){
       ofFile recentProjectsFile(ofToDataPath("recent.txt"), ofFile::ReadWrite);
       recentProjectsFile.create();
       ofLogNotice("Creado archivo de recientes porque no existía...");
    }else{
        recent = ofSplitString(ofBufferFromFile("recent.txt").getText(), "\n");
    }
    //eliminá las líneas vacías
    for(unsigned int i = 0 ; i < recent.size() ; i ++){
        if(recent[i].size() == 0){
            recent.erase(recent.begin() + i);
        }
    }

    //std::reverse(recent.begin(),recent.end());
    return recent;

}

void SessionManager::saveToRecentProject(string path){
    //Si no hay recientes
    if(recentProjects.size() == 0){
        recentProjects.push_back(path);
    } else {

        for(unsigned int i = 0 ; i < recentProjects.size(); i++){
            if(path == recentProjects[i]){
                recentProjects.erase( recentProjects.begin()+i );
                break;
            }
        }

        recentProjects.push_back(path);
    }
}


void SessionManager::saveSession() {
    ofLog() << "Saving...";

    /****
        Sounds
    ****/

    Json::Value jsonSounds = jsonFile["sounds"];

    if ( jsonSounds == Json::nullValue ) {
        jsonFile["sounds"] = Json::Value( Json::objectValue );
    }

    jsonFile["sounds"] = sounds->save();


    /****
        MODES
    ****/

    Json::Value jsonModes = jsonFile["modes"];

    if ( jsonModes == Json::nullValue ) {
        jsonFile["modes"] = Json::Value( Json::objectValue );
    }

    for ( int i = 0 ; i < modes->modeNames.size() ; i++ ) {
        jsonFile["modes"][ modes->modeNames[i] ] = modes->modes[i]->save();
    }

    /****
        MIDI CC
    ****/

    Json::Value jsonMidiCC = jsonFile["midiCC"];

    if ( jsonMidiCC == Json::nullValue ){
      jsonFile["midiCC"] = Json::Value( Json::arrayValue);
    }

    jsonFile["midiCC"] = midiServer->save();

    jsonFile.save(jsonFilename, true);

    ofLog() << "Done saving";
}

void SessionManager::saveAsNewSession(){
  ofFileDialogResult result = ofSystemSaveDialog(
              ofFilePath::getEnclosingDirectory(jsonFilename) + "/new_session.json", "Save");

  if(result.bSuccess){
    string path = result.getPath();

    //Seteando el nombre de vuelta --> OJO
    jsonFilename = path;
    saveSession();
    setWindowTitleWithSessionName();
    saveToRecentProject(jsonFilename);
  }
}


vector <string> SessionManager::getRecentProjects(){
        vector<string> invertedRecent;
        //std::reverse_copy(recentProjects.begin(), recentProjects.end(), invertedRecent.begin());
        for(int i = recentProjects.size() - 1; i >= 0; i --){
                invertedRecent.push_back(recentProjects[i]);
        }
        return invertedRecent;
}

bool SessionManager::getSessionStarted(){
        return sessionStarted;
}

bool SessionManager::areAnyRecentProjects(vector<string> recentProjects){
   return recentProjects.size() > 0 && recentProjects[recentProjects.size() - 1] != "";
}

//On Exit
void SessionManager::exit(){
   ofBuffer recent;

   for(unsigned int i = 0 ; i < recentProjects.size(); i++){
       string s = "";

       if(i != 0){
           s = "\n" + recentProjects[i];
       }else{
           s = recentProjects[i];
       }

       recent.append(s.c_str(), s.size());
   }


   ofBufferToFile("recent.txt", recent);
   ofLogNotice("Written recent files");

}

void SessionManager::setWindowTitleWithSessionName(){
    ofSetWindowTitle("AudioStellar // " + ofFilePath::getFileName( jsonFilename ) );
    }
