#include "Tooltip.h"

Tooltip::tooltip Tooltip::Tooltip::SOUNDS_VOLUME = { "Volume", "Set the master volume." };
Tooltip::tooltip Tooltip::SOUNDS_REPLAY = { "Replay Sound", "When active allows to retrigger sounds as they are clicked." };
Tooltip::tooltip Tooltip::SOUNDS_ORIGINAL_POSITIONS = { "Original Positions", "Revert sounds to their original positions." };

Tooltip::tooltip Tooltip::CLUSTERS_EPS = { "Cluster Eps", "DBSCAN Epsilon parameter specifies how close points should be to each other to be considered as part of a cluster." };
Tooltip::tooltip Tooltip::CLUSTERS = { "Clusters", "Click to activate or deactivate cluster." };
Tooltip::tooltip Tooltip::CLUSTERS_EXPORT_FILES  = { "Export files", "Export sounds in each cluster to folders." };

Tooltip::tooltip Tooltip::SESSION_SAVE = { "Save Session", "Saves all current settings." };

Tooltip::tooltip Tooltip::MODE_EXPLORER = { "Explorer Mode", "Play sounds in the map by cliking on them. Right click on each sound to get options." };
Tooltip::tooltip Tooltip::MODE_PARTICLE = { "Particle Mode", "Click on the map to drop particles. Each time a particle collides with a sound it triggers playback. Choose different particle modes and adjust parameters in the Tools menu to set particle trajectories." };
Tooltip::tooltip Tooltip::MODE_SEQUENCE = { "Sequence Mode", "Select sounds in the map to play them sequentially, in a loop. Sounds are played in order, as they are added to the sequence. The time interval between sounds in the sequence is related to their distances in the map." };

Tooltip::tooltip Tooltip::PARTICLE_AGE = { "Age", "Set the lifespan of each particle. A value of 10 makes the particle permanent." };
Tooltip::tooltip Tooltip::PARTICLE_RANDOMIZE_EMITTER = { "Randomize Emitter", "" };

Tooltip::tooltip Tooltip::SIMPLE_MODEL = { "Simple", "Simple particles move in straight line" };
Tooltip::tooltip Tooltip::SIMPLE_X_ACCELERATION = { "X", "Sets the acceleration in the X axis. Negative values invert direction." };
Tooltip::tooltip Tooltip::SIMPLE_Y_ACCELERATION = { "Y", "Sets the acceleration in the Y axis. Negative values invert direction." };

Tooltip::tooltip Tooltip::SWARM_MODEL = { "Swarm", "Swarm particles perform a two dimensional random walk. They take a step each time on a random direction." };
Tooltip::tooltip Tooltip::SWARM_X_ACCELERATION = { "X", "Sets the velocity in the X axis. Negative values invert direction." };
Tooltip::tooltip Tooltip::SWARM_Y_ACCELERATION = { "Y", "Sets the velocity in the Y axis. Negative values invert direction." };
Tooltip::tooltip Tooltip::SPREAD_AMOUNT = { "Spread Amount", "Set the size of each step." };

Tooltip::tooltip Tooltip::EXPLOSION_MODEL = { "Explosion", "Explosion particles move in stright line in all directions away from an emitter. Click on the map to create a particle emitter" };
Tooltip::tooltip Tooltip::EXPLOSION_SPEED = { "Speed", "Set the speed for radial particles." };
Tooltip::tooltip Tooltip::EXPLOSION_DENSITY = { "Density", "Set the number of radial particles created from each emmitter." };

Tooltip::tooltip Tooltip::ATTRACTOR_GRAVITY = { "Gravity", "" };
Tooltip::tooltip Tooltip::ATTRACTOR_MASS = { "Mass", "" };

Tooltip::tooltip Tooltip::SEQUENCE_BPM = { "BPM", "Set the tempo in beats per minute." };

Tooltip::tooltip Tooltip::SEQUENCE_ACTIVE = { "Select sequence track", "Click to edit sequence parameters." };
Tooltip::tooltip Tooltip::SEQUENCE_VOLUME = { "Sequence Volume", "Set the volume for each sequence track." };
Tooltip::tooltip Tooltip::SEQUENCE_OFFSET = { "Offset", "Set the offset time from the starting beat in sixteenth notes resolution" };
Tooltip::tooltip Tooltip::SEQUENCE_PROBABILITY = { "Probability", "Set the probability to be played for all sounds in the sequence." };
Tooltip::tooltip Tooltip::SEQUENCE_BARS = { "Bars", "Set the duration of the sequence." };
Tooltip::tooltip Tooltip::SEQUENCE_CLEAR = { "Clear sequence", "Delete all the events in the sequence." };

Tooltip::tooltip Tooltip::MIDI_DEVICES = { "Midi Devices", "Select available Midi devices." };
Tooltip::tooltip Tooltip::MIDI_LEARN = { "Midi Learn", "When active any Midi note or control message received will be assigned to a selected sound in the map." };
Tooltip::tooltip Tooltip::MIDI_CLOCK = { "Use MIDI clock", "Get the tempo from an external MIDI device." };


Tooltip::tooltip Tooltip::DIMREDUCT_AUDIO_SAMPLE_RATE = { "Sample rate", "" };
Tooltip::tooltip Tooltip::DIMREDUCT_AUDIO_DURATION = { "Duration", "Set the duration of the audio samples in seconds. If your files are longer they will be chopped otherwise they will be zero padded to the specified duration." };
Tooltip::tooltip Tooltip::DIMREDUCT_STFT = { "Short Time Fourier Transform", "" };
Tooltip::tooltip Tooltip::DIMREDUCT_STFT_WINDOW_SIZE = { "Window size", "Set the size of the analysis in number of samples. Smaller windows produce more precise time resolution while larger windows increase frequency resolution" };
Tooltip::tooltip Tooltip::DIMREDUCT_STFT_HOP_SIZE = { "Hop size", "Set the offset for the overlapping windows." };
Tooltip::tooltip Tooltip::DIMREDUCT_PCA = { "Principal Component Analysis", "PCA performs a linear mapping of data to a lower-dimensional space in such a way that the variance of the data in the low dimensional representation is maximized." };
Tooltip::tooltip Tooltip::DIMREDUCT_PCA_COMPONENTS = { "Components", "Set the number of components." };
Tooltip::tooltip Tooltip::DIMREDUCT_TSNE = { "T-SNE", "" };
Tooltip::tooltip Tooltip::DIMREDUCT_TSNE_PERPLEXITY = { "Perplexity", "Set the number of effective nearest neighbors." };
Tooltip::tooltip Tooltip::DIMREDUCT_TSNE_THETA = { "Theta", "The parameter Theta specifies how coarse the Barnes-Hut approximation is." };



