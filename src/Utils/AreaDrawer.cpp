#include "AreaDrawer.h"

AreaDrawer::AreaDrawer(ofVec2f _initP){
    initialPoint = _initP;
    shouldKeepArea = false;
}


ofVec2f AreaDrawer::getInitialPoint(){
    return initialPoint;
}


//------------MANAGER--------------//
AreaDrawerManager::AreaDrawerManager(){
    status = "idle";
    ofSetLineWidth(1);

}
void AreaDrawerManager::update(ofVec2f p){
    if(prevArea != NULL){
      if(prevArea->isClosed() && !prevArea->shouldKeepArea){
        prevArea->clear();
      }
    }
    if(status == "idle"){
        prevArea = area;
        area = NULL;
        area = new AreaDrawer(p);
        areas.push_back(area);
        status = "drawing";
    }else if(status == "drawing"){
        if(area->getVertices().size() > 20){
            if(Utils::distance(p,area->getInitialPoint()) < 10){
                area->close();

                status = "closedButNoRelease";
                return;
            }
        }
        //area->lineTo(p.x, p.y);
        area->curveTo(p.x,p.y);
    }
}

void AreaDrawerManager::draw(){
    for(unsigned int i = 0; i<areas.size();i++){
        if(areas[i]->isClosed()){
            ofSetColor(ofColor::cyan);
        }else{
            ofSetColor(ofColor::white);
        }
        areas[i]->draw();
    }
}
void AreaDrawerManager::mouseReleased(ofVec2f p){
    status = "idle";

    if(!area->isClosed()){
        area->clear();
    }

}

ofPoint AreaDrawerManager::getCentroid(){
    return area->getCentroid2D();
}
float AreaDrawerManager::getArea(){
    return area->getArea();
}
string AreaDrawerManager::getStatus(){
    return status;
}
void AreaDrawerManager::noteSetted(bool s){
    area->shouldKeepArea = s;
}
