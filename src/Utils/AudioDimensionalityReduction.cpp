#include "AudioDimensionalityReduction.h"
string AudioDimensionalityReduction::currentLog = "";
string AudioDimensionalityReduction::currentProcess = "";

float AudioDimensionalityReduction::progress = 0;
int AudioDimensionalityReduction::filesFound = 0;
string AudioDimensionalityReduction::generatedJSONPath = "";


vector< vector<float> > AudioDimensionalityReduction::doSTFT(vector<float> audioData, int windowSize, int hopSize)
{
    /*
     * Esta implementación tiene diferencias con la librosa.
     * En caso que se quiera hacer exactamente igual:
     * librosa hace "centered": np.pad(audioData,windowSize/4,"reflect")
     * esto hace que el array resultante sea un poco mas largo
     * además agrega 1 más que es el offset
     */
    
    kiss_fftr_cfg cfg = kiss_fftr_alloc( windowSize, 0 , NULL, NULL );
    
    vector< vector<float> > stft;
    int i;
    
    // Create a hamming window of appropriate length
    float window[windowSize];
    hamming(windowSize, window);
    float windowSum = 0;
    for ( i = 0 ; i < windowSize ; i++ ) {
        windowSum += window[i];
    }
    
    int chunkPosition = 0;
    int readIndex;
    bool bStop = false;
    kiss_fft_cpx fftResult[windowSize/2];
    
    // Process each chunk of the signal
    while(chunkPosition < audioData.size() && !bStop) {
        
        float windowedAudio[windowSize];
        // Copy the chunk into our buffer
        for(i = 0; i < windowSize; i++) {
            readIndex = chunkPosition + i;
            
            if(readIndex < audioData.size()) {
                windowedAudio[i] = audioData[readIndex] * window[i];
            } else {
                // we have read beyond the signal, so zero-pad it!
                windowedAudio[i] = 0.0;
                bStop = true;
            }
        }
        
        kiss_fftr(cfg, (kiss_fft_scalar*)windowedAudio, fftResult);
        vector<float> fftResultReal;
        float normalizer = 1. / pow(windowSum,2); // asi lo hace scipy
        
        
        for ( i = 0 ; i < windowSize / 2 ; i++ ) {
            fftResultReal.push_back( sqrtf(fftResult[i].r * fftResult[i].r + fftResult[i].i * fftResult[i].i) * normalizer );
        }
        
        stft.push_back( fftResultReal );
        
        chunkPosition += hopSize;
    }
    
    kiss_fftr_free(cfg);
    
    return stft;
}

vector<vector<float> > AudioDimensionalityReduction::doSTFTAll(vector<vector<float> > data, int windowSize, int hopSize)
{
    vector<vector<float>> stftVectors;
    for ( auto audioData : data ) {
        vector< vector<float> > stftData = doSTFT(audioData, windowSize, hopSize);
        
        //vectorization
        vector<float> stftVectorized;
        for ( auto stftTimeWindow : stftData ) {
            for ( auto freqAmplitude : stftTimeWindow ) {
                stftVectorized.push_back(freqAmplitude);
            }
        }
        
        stftVectors.push_back( stftVectorized ); //esto debería dar 45100
        progress += 0.4f / (data.size() + 1);
    }
    return stftVectors;
}

vector<vector<float> > AudioDimensionalityReduction::doPCA(vector<vector<float> > data, int nComponents)
{
    vector<vector <float>> pcaResult;
    
    auto timeStart = ofGetElapsedTimeMillis();
    cout << "PCA (" << nComponents << " components)..." << endl;
    ofxPCA pca;
    cout << data.size() << endl;
    pca.calculate(data,nComponents); //aca hay mas parametros que hay que chequear que este todo ok
    cout << data.size() << endl;
    ofExit();
    float variabilidad = 0;
    for ( auto v : pca.prop_of_var() ) {
        variabilidad += v;
        cout << variabilidad << endl;
    }
    
    cout << "components: " << pca.prop_of_var().size() << " variabilidad: " << variabilidad << " || Threshold 95: " << pca.thresh95() << " keiser: " << pca.kaiser() << endl;
    cout << "finished (" << ofGetElapsedTimeMillis() - timeStart << " ms)" << endl; //198,059 segundos | 3.3 minutos
    
    for ( auto v : data ) {
        ofLog() << "transforming";
        pcaResult.push_back( pca.transform(v) );
    }
    
    return pcaResult;
}

vector<vector<double> > AudioDimensionalityReduction::doTSNE(vector<vector<float> > data, double perplexity, double theta)
{
    ofxTSNE tsne;
    ofLog() << "tSNE...";
    auto timeStart = ofGetElapsedTimeMillis();
    currentProcess = "Measuring similarity between all sounds...";
    auto result = tsne.run( data, 2, perplexity, theta, true, true );
    currentProcess = "Iterating t-SNE...";
    for (int i = 0; i < 1000; i++) {
        result = tsne.iterate();
        progress += 0.2f / (1000 + 1);
    }
    ofLog() << "finished (" << ofGetElapsedTimeMillis() - timeStart << " ms)";
    return result;
}

string AudioDimensionalityReduction::run(string dirPath,
                                         int targetSampleRate,
                                         float targetDuration,
                                         int stft_windowSize,
                                         int stft_hopSize,
                                         int pca_nComponents,
                                         double tsne_perplexity,
                                         double tsne_theta) {
    
    progress = 0;
    
    if (dirPath.size() > 0) {
        
        vector<string> audioFilesPaths = searchAudioFiles(dirPath);
        filesFound = audioFilesPaths.size();
        ofLog() << audioFilesPaths.size() << " files found";
        
        currentProcess = "Loading files...";
        vector<vector<float>> rawAudios = loadFilesAndCrop(audioFilesPaths, targetSampleRate, targetDuration);
        
        progress = 0.4f;
        currentProcess = "Extracting features...";
        vector<vector<float>> stftVectors = doSTFTAll(rawAudios, stft_windowSize, stft_hopSize);
        
        progress = 0.8f;
        // NO PCA !! :(( Es muuuuy lento pero anda en linux
        //        currentProcess = "Running PCA...";
        //        vector<vector<float>> pcaResult = doPCA(stftVectors, pca_nComponents);
        
        currentProcess = "Running t-SNE...";
        //        vector<vector<double>> tsneResult = doTSNE(pcaResult, tsne_perplexity, tsne_theta);
        vector<vector<double>> tsneResult = doTSNE(stftVectors, tsne_perplexity, tsne_theta);
        
        currentProcess = "";
        progress = 1.0;
        generatedJSONPath = createJson(dirPath, tsneResult, audioFilesPaths);
    }
    
    return "";
}

vector<string> AudioDimensionalityReduction::searchAudioFiles(string dirPath, vector<string> extensions)
{
    ofDirectory dir ( dirPath );
    vector<string> files;
    scanDir(dir, files, extensions);
    
    return files;
}

void AudioDimensionalityReduction::hamming(int windowLength, float *buffer) {
    for(int i = 0; i < windowLength; i++) {
        buffer[i] = 0.54 - (0.46 * cos( 2 * PI * (i / ((windowLength - 1) * 1.0))));
    }
}

void AudioDimensionalityReduction::scanDir(ofDirectory dir, vector<string> &files, vector<string> extensions)
{
    dir.listDir();
    
    for(auto file : dir)
    {
        if(file.isDirectory())
        {
            scanDir(ofDirectory(file.getAbsolutePath()), files, extensions);
        }
        else
        {
            string extension = ofToLower( file.getExtension() );
            if ( std::find(extensions.begin(), extensions.end(), extension) != extensions.end() ) {
                files.push_back(file.path());
            }
        }
    }
}

vector<float> AudioDimensionalityReduction::loadFileAndCrop(string filePath, int targetSampleRate, float targetDuration)
{
    vector<float> audioData;
    
    ofxAudioFile audio;
    audio.load( filePath );
    
    if ( !audio.loaded() ) {
        ofLog() << "ERROR: Could not load file " << filePath;
        return audioData;
    }
    if ( audio.samplerate() < targetSampleRate ) {
        ofLog() << "ERROR: Sample rate of file " << filePath << " is " << audio.samplerate() << ". This is lower than target: " << targetSampleRate;
        return audioData;
    }
    if ( audio.samplerate() % targetSampleRate != 0 ) {
        ofLog() << "ERROR: Sample rate of file " << filePath << " is " << audio.samplerate() << ". This must be divisible by target: " << targetSampleRate;
        return audioData;
    }
    
    for ( int i = 0 ; i < audio.length() ; i += audio.samplerate() / targetSampleRate ) {
        audioData.push_back( audio.sample(i,0) );
    }
    
    string strLog = "";
    
    float currentDuration = audioData.size() / (float)targetSampleRate;
    
    if ( currentDuration > targetDuration ) {
        audioData.resize( targetSampleRate * targetDuration );
    } else if ( currentDuration < targetDuration ) {
        while( currentDuration < targetDuration ) {
            audioData.push_back(0);
            currentDuration = audioData.size() / targetSampleRate;
        }
    }
    
    return audioData;
}

vector<vector<float>> AudioDimensionalityReduction::loadFilesAndCrop(vector<string> filePaths, int targetSampleRate, float targetDuration)
{
    float currentFileCount = 0;
    vector< vector<float> > dataReturn;
    for ( auto audioFilePath : filePaths ) {
        vector<float> audioData = loadFileAndCrop(audioFilePath, targetSampleRate, targetDuration);
        if ( audioData.size() > 0 ) {
            dataReturn.push_back( audioData );
        }
        
        currentFileCount += 1.0f;
        progress = 0.4f * ( currentFileCount/filesFound );
    }
    return dataReturn;
}


string AudioDimensionalityReduction::selectFolder() {
    
    ofFileDialogResult dialogResult = ofSystemLoadDialog("Select Folder", true, ofFilePath::getUserHomeDir());
    
    string path;
    
    if(dialogResult.bSuccess) {
        path = dialogResult.getPath();
    }
    
    return path;
    
}

void AudioDimensionalityReduction::threadedFunction()
{
    if ( isThreadRunning() ) {
        run(dirPath,
            targetSampleRate,
            targetDuration,
            stft_windowSize,
            stft_hopSize,
            pca_nComponents,
            tsne_perplexity,
            tsne_theta);
    }
}

string AudioDimensionalityReduction::createJson(string dirPath, vector<vector<double>> tsneResult, vector<string> audioFilesPaths) {
    
    vector<string> pointsData;
    
    for (int i = 0; i < tsneResult.size(); i++) {
        
        vector<string> data;
        
        string audioFilePath = ofFilePath::makeRelative(dirPath, audioFilesPaths[i]);
        
        data.push_back(ofToString(tsneResult[i][0])); // x
        data.push_back(ofToString(tsneResult[i][1])); // y
        data.push_back(ofToString(0)); // z
        data.push_back(ofToString(0)); // cluster
        data.push_back(ofToString(audioFilePath)); // path
        
        string pointData = ofJoinString(data, ",");
        
        pointsData.push_back(pointData);
        
    }
    
    string tsvString = ofJoinString(pointsData, "|");
    
    string path;
    path = ofFilePath::getFileName(dirPath);
    path = ofFilePath::getPathForDirectory(path);
    
    string jsonFilename;
    jsonFilename = dirPath + ".json";
    
    ofxJSONElement jsonFile;
    Json::Value value = Json::Value( Json::objectValue );
    
    value["audioFilesPath"] = path;
    value["tsv"] = tsvString;
    
    jsonFile = value;
    
    jsonFile.save(jsonFilename, true);
    
    return jsonFilename;
}

