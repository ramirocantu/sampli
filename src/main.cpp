#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( int argc, char *argv[] ) {

    ofSetupOpenGL(1280,720, OF_WINDOW);			// <-------- setup the GL context
//    ofSetupOpenGL(800,600, OF_FULLSCREEN);			// <-------- setup the GL context
    ofApp *app = new ofApp();

    app->arguments = vector<string>(argv, argv + argc);

    ofRunApp(app);
}

