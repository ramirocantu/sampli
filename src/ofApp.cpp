#include "ofApp.h"
//--------------------------------------------------------------
void ofApp::setup() {

    #ifdef TARGET_OS_MAC
        ofSetDataPathRoot("../Resources/data/");
    #endif
    
    ofSetWindowTitle("AudioStellar");
    ofSetEscapeQuitsApp(false);

    ofEnableAlphaBlending();
    ofSetVerticalSync(true);

    sounds = new Sounds();
    midiServer = new MidiServer();
    modes = new Modes(sounds, midiServer);
    sessionManager = new SessionManager(modes, sounds, midiServer);
    gui = new Gui(sounds, midiServer,  modes, sessionManager);

    midiServer->init(modes);
    cam.init();

    //check if json passed from command line
    #ifndef TARGET_OS_MAC
    if(arguments.size() > 1){
        sessionManager->loadSession(arguments);
    }
    #endif
}

//--------------------------------------------------------------
void ofApp::update() {
    sounds->update();
    modes->update();
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofBackground(25);

    if ( useCam ) {
        cam.begin();
    }

    modes->beforeDraw();
    sounds->draw();
    modes->draw();

    if ( useCam ) {
        cam.end();
    }

    gui->draw();
}

void ofApp::keyPressed(int key) {
    gui->keyPressed(key);
    modes->keyPressed(key);

}

void ofApp::mousePressed(int x, int y, int button) {
    if(!gui->isMouseHoveringGUI()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        modes->mousePressed( realCoordinates.x,
                             realCoordinates.y,
                             button );
    }
}

void ofApp::mouseMoved(int x, int y) {
    if(sessionManager->getSessionStarted()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        if(!gui->isMouseHoveringGUI()){
            sounds->mouseMoved(realCoordinates.x,
                               realCoordinates.y,
                               false);
        }
    }
}

void ofApp::mouseDragged(int x, int y, int button) {
    if(!gui->isMouseHoveringGUI()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        modes->mouseDragged(realCoordinates.x,
                            realCoordinates.y,
                            button);

        sounds->mouseDragged(realCoordinates, button);
    }

}

void ofApp::mouseReleased(int x, int y, int button) {
    if(!gui->isMouseHoveringGUI()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        modes->mouseReleased(realCoordinates.x,
                             realCoordinates.y,
                             button);
    }
}

void ofApp::keyReleased(int key) {
    if ( key == 'c' ) {
        useCam = !useCam;
    }

}
void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY) {
    // cam.mouseScrolled(x,y,scrollX,scrollY);
}

void ofApp::exit(){
    sessionManager->exit();
    ofLogNotice("EXIT");
}
