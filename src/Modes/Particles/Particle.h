#ifndef _PARTICLE
#define _PARTICLE

#include "ofMain.h"
#include "Utils/ParticleRegions.h"
#include "Sound.h"


class Particle {
private:
    ofFbo fbo;

public:
    float lifeSpan;
    float age;
    ofVec2f position;
    float size;
    bool dTrace = false;
    Sound * lastPlayedSound = NULL;

    //por ahora sólo te usa el modo atractor
    float mass = 2;

    //a q región perteneces, si es que existe
    ParticleRegion* region = NULL;

    Particle() {
        position.set(ofGetWidth() / 2, ofGetHeight() / 2);
        lifeSpan = 255;
        size = 1.5;
        if(dTrace){
            fbo.allocate(ofGetWidth(),ofGetHeight(), GL_RGBA);
            fbo.begin();
            ofClear(255,255,255,0);
            fbo.end();
        }
    }


    virtual void customUpdate() {}
    virtual void customDraw() {}
    virtual void applyForce(ofVec2f force){}


    void update() {
        customUpdate();
        if (age != 10.0f) {
            lifeSpan -= ofMap(age,0,10,10,0)/2;
        }
    }

    void draw() {
        if(dTrace){
            drawTrace();
            fbo.draw(0,0);
        }else{
            ofFill();
            ofSetColor(232,11,85, lifeSpan );//fill color
            ofDrawCircle(position.x, position.y, size);

        }


    };

    void drawTrace(){
        fbo.begin();
        ofFill();
        ofSetColor(255,255,255, 5);
        ofDrawRectangle(0,0,ofGetWidth(), ofGetHeight());
        ofSetColor(255, 0, 255, lifeSpan );//fill color
        ofDrawCircle(position.x, position.y, size);
        fbo.end();
    }

    bool isDead() {
      bool mainCondition = (lifeSpan <= 0.0 || position.x > ofGetWidth() || position.x < 0 ||
                position.y > ofGetHeight() || position.y < 0);

      if(region != NULL){
        return !isInsideRegion() || mainCondition;
      }
      return mainCondition;
    };

    bool isInsideRegion(){
      ofVec2f centerPoint = region->centerPoint;
      float dist = ofDist(position.x, position.y, centerPoint.x, centerPoint.y);
      return dist <= fabs(region->radius);
    };

    void kill(){
        lifeSpan = 0.0;
    };

    /*
    string getModelName(){
        return modelName;
    }
    */

    ofVec2f getPosition() {
        return position;
    }
    void setPosition(ofVec2f p) {
        position = p;
    }
    float getLifeSpan() {
        return lifeSpan;
    }
    //asignar area de nota midi a particula
    void setArea(ParticleRegion* r){
      region = r;
    }

    bool crashes(ofVec2f pos){
       int threshold = 10;
       float dist = ofDist(position.x, position.y, pos.x, pos.y);

       return dist <= fabs(threshold);
    }


};



#endif
