#pragma once

#include "ofMain.h"
#include "Sounds.h"
#include "ParticleMode.h"
#include "ExplorerMode.h"
#include "SequenceMode.h"
#include "Mode.h"
#include "Utils.h"
#include "ImageButton.h"
#include "MidiServer.h"
#include "ColorPaletteGenerator.h"
#include "ofxImGui.h"
#include "Tooltip.h"

class MidiServer;

class Modes {
private:
    GLuint loadModeIcon(const string & filePath);
    Mode *atMode;

    ofxImGui::Gui gui;
    vector<GLuint> modesIcons;

    Sounds *sounds = NULL;
    MidiServer *midiServer = NULL;

//    ofxDatGuiDropdown *modeSelector;

public:
    vector<Mode*> modes;
    vector<string> modeNames;
    vector<Tooltip::tooltip> modeTooltips;


    Modes(Sounds *_sounds, MidiServer *_midiServer);
    void initModes();
//    void onGuiEvent(ofxDatGuiDropdownEvent e);
    void beforeDraw();
    void draw();
    //void drawGui(); //se separó en dos funciones
    void drawModeSelector();
    void drawModesSettings();
    void update();
    void mousePressed(int x, int y, int button);
    void keyPressed(int key);
    void mouseDragged(int x, int y, int button);
    void mouseReleased(int x, int y , int button);
    void mouseMoved(int x, int y);
    void midiMessage(Utils::midiMsg m);
    Mode* getActiveMode();
    string getActiveModeName();


};
